"use strict";
var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var del = require("del");
var runSequence = require("run-sequence");
var sass = require("gulp-sass");
var pug = require('gulp-pug');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var gutil = require('gulp-util');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var cssnano = require('cssnano');
var browserSync = require('browser-sync').create();


gulp.task("html", function () {
    return gulp.src("./app/*.pug")
        .pipe(pug())
        .pipe(gulp.dest("./dist/"));
});

gulp.task("js", function () {
    var b = browserify({
        entries: './app/js/letters.js',
        debug: true
    });

    return b.bundle()
        .pipe(source('main.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        // Add transformation tasks to the pipeline here.
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js/'));

});

gulp.task("css", function () {
    var processors = [
        autoprefixer({browsers: ['last 2 versions']}),
        cssnano()
    ];

    return gulp.src("./app/sass/main.sass")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("./dist/css/"));
});

gulp.task("clean", function () {
    return del.sync("./dist");
});

gulp.task("watch", function () {
    runSequence("clean", ["js", "html", "css"]);

    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch("./app/js/**/*.js", ["js"]).on("change", browserSync.reload);
    gulp.watch("./app/*.pug", ["html"]).on("change", browserSync.reload);
    gulp.watch("./app/sass/main.sass", ["css"]).on("change", browserSync.reload);
});

gulp.task("default", function () {
    runSequence("clean", ["js", "html", "css"]);
});