"use strict";
var staple = new (require("./staple.js"))({drawGrid: true, strokeWidth: 1, waitTime: 100});

window.onkeypress = function (e) {
    staple.writeText(String.fromCharCode(e.which));
};

window.onkeydown = function ESCClearScreen(e) {
    if (e.keyCode === 27) {
        staple.clear();
    }
};