"use strict";
var SVG = require("svg.js");

module.exports = function (params) {
    var config = Object.assign({
        "node_id": "drawing",
        "strokeWidth": 2,
        "stapleSize": 20,
        "scale": 1,
        "xOffset": 1,
        "yOffset": 1,
        "bg_color": "#eee",
        "bg_opacity": 0.1,
        "grid_color": "#ddd",
        "grid_width": 0.3,
        "staple_colors": ["#652829", "#1C711C", "#24244C"],
        "waitTime": 1000,
        "duration": 250,
        "canvas_width": 300,
        "canvas_height": 300,
        "drawGrid": false
    }, params);

    var coordinates = "0," + config.stapleSize * config.scale / 2 +
        " 0,0 " + config.stapleSize * config.scale +
        ",0 " + config.stapleSize * config.scale +
        "," + config.stapleSize * config.scale / 2;

    var draw = new SVG(config.node_id).size(config.canvas_width, config.canvas_height);

    var xOffset = config.xOffset;
    var yOffset = config.yOffset;
    var letters = {
        "А": [{"move": [0, 2]}, {"rotate": 180, "move": [0, 2]}, {"move": [0, -4]}],
        "Б": [{"rotate": -90, "move": [-3, -1]}, {}, {"rotate": 180, "move": [0, 4]}],
        "В": [{"move": [0, 2]}, {"rotate": 180, "move": [0, 2]}, {"rotate": 180, "move": [0, 4]}],
        "Г": [{"move": [0, 2]}, {"rotate": -90, "move": [1, -1]}, {"rotate": 90, "move": [-5, 1]}],
        "Д": [{"rotate": -90, "move": [-1, -1]}, {"rotate": 90, "move": [-3, 1]}, {"move": [0, -4]}],
        "Е": [{"move": [0, 2]}, {"rotate": 180, "move": [0, 2]}, {"rotate": -90, "move": [5, -1]}],
        "Ё": [{"move": [0, 2]}, {"rotate": 180, "move": [0, 2]}, {"rotate": -90, "move": [5, -1]}],
        "Ж": [{"rotate": 90, "move": [3, 1]}, {"rotate": -90, "move": [1, 1]}, {"rotate": -90, "move": [5, 3]}],
        "З": [{}, {"rotate": 90, "move": [-1, -1]}, {"rotate": 180, "move": [0, 4]}],
        "И": [{"rotate": -90, "move": [-3, -1]}, {"rotate": 180}, {"rotate": -90, "move": [5, 3]}],
        "Й": [{"rotate": -90, "move": [-3, -1]}, {"rotate": 180}, {"rotate": -90, "move": [5, 3]}],
        "К": [{"rotate": 180}, {"rotate": -90, "move": [1, -1]}, {"move": [0, -4]}],
        "Л": [{"rotate": 90, "move": [3, 1]}, {"move": [2, -2]}, {"rotate": 90, "move": [-5, -3]}],
        "М": [{"rotate": 90, "move": [3, 1]}, {"move": [0, -2]}, {"rotate": 90, "move": [-5, -3]}],
        "Н": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "О": [{"move": [0, 2]}, {"rotate": -90, "move": [1, -1]}, {"rotate": 90, "move": [-5, -1]}],
        "П": [{"move": [0, 2]}, {"rotate": 90, "move": [-1, 1]}, {"rotate": -90, "move": [5, 3]}],
        "Р": [{}, {"rotate": 180, "move": [0, 4]}, {"rotate": -90, "move": [5, -1]}],
        "С": [{"rotate": 90, "move": [3, 1]}, {"rotate": -90, "move": [1, -1]}, {"rotate": -90, "move": [5, 1]}],
        "Т": [{"move": [0, 2]}, {"rotate": 90, "move": [-1, 1]}, {"rotate": -90, "move": [5, 1]}],
        "У": [{"rotate": 180}, {"rotate": 90, "move": [-1, -1]}, {"rotate": 180, "move": [0, 4]}],
        "Ф": [{"rotate": -90, "move": [-1, -1]}, {"rotate": 90, "move": [-3, -1]}, {"rotate": 90, "move": [-5, 1]}],
        "Х": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ц": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ч": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ш": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Щ": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ь": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ы": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ъ": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Э": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Ю": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}],
        "Я": [{"rotate": 90, "move": [3, 1]}, {"move": [2, 0]}, {"rotate": 90, "move": [-5, -3]}]
    };

    function drawGrid() {
        draw.rect(config.canvas_width, config.canvas_height).attr({
            fill: config.bg_color, "fill-opacity": config.bg_opacity
        });
        var i;
        var gridParam = {width: config.grid_width, color: config.grid_color};
        for (i = 0; i <= config.canvas_width; i += (config.stapleSize / 2)) {
            draw.line(i, 0, i, config.canvas_height).stroke(gridParam);
        }
        for (i = 0; i <= config.canvas_height; i += (config.stapleSize / 2)) {
            draw.line(0, i, config.canvas_width, i).stroke(gridParam);
        }
    }

    function countColumn() {
        xOffset += 2;
        if ((xOffset + 1.5) * config.stapleSize > config.canvas_width) {
            xOffset = 1;
            yOffset += 6;
        }
    }

    function drawLetter(params) {
        if (!params || params.length !== 3) {
            if (xOffset > 1) {
                countColumn();
            }
            return false;
        }
        var offset = 0;
        var k;
        var skrepa;
        var wait = config.waitTime;
        for (k = 0; k < params.length; k += 1) {
            skrepa = draw.polyline(coordinates)
                .fill("none")
                .stroke({width: config.strokeWidth, color: config.staple_colors[k]})
                .move(xOffset * config.stapleSize, yOffset * config.stapleSize / 2 + config.stapleSize / 2 * offset);
            wait = config.waitTime;
            if (params[k].rotate !== undefined) {
                skrepa.animate(config.duration, ">", wait).rotate(params[k].rotate);
                wait = 0;
            }
            if (params[k].move !== undefined) {
                skrepa.animate(config.duration, ">", wait)
                    .dmove(config.stapleSize * params[k].move[0] / 4, config.stapleSize * params[k].move[1] / 4);
            }
            offset += 2;
        }
        countColumn();
    }

    this.writeText = function (text) {
        if (text === undefined || text.length === 0) {
            return this;
        }
        text = "" + text.toUpperCase();
        var l;
        for (l = 0; l < text.length; l += 1) {
            drawLetter(letters[text[l]]);
        }
        return this;
    };

    this.clear = function () {
        xOffset = config.xOffset;
        yOffset = config.yOffset;
        draw.clear();
        if (config.drawGrid) {
            drawGrid();
        }
    };

    this.clear();
};
